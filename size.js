const gzip_size = require("gzip-size")

gzip_size.file("./build/veedom.min.js").then(size => {
    console.log(`Resulting build size, minified/gzipped: ${size}`)
})
