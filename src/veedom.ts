declare global {
    namespace JSX {
        interface Element extends VNode { }
        interface IntrinsicElements {
            [elemName: string]: JSXProps
        }
    }
}

interface TargetElement extends Element {
    _vNode: VNode
    _removed?: boolean
}

type TargetNode = TargetElement | Text

interface JSXAttrs {
    oncreate?: { (el: Element): void }
    onupdate?: { (el: Element): void }
    onremove?: { (destroy: Function): void }
    ondestroy?: { (node: VNode): void }
}

interface JSXProps<T extends Element = Element> extends JSXAttrs {
    [any: string]: any
}

export interface PropMap extends JSXAttrs {
    [name: string]: string | number | boolean | Function | null | undefined
}

export interface VNode<TProps extends PropMap = PropMap> {
    type: string
    props: TProps
    key?: string
    children: VNodeList
}

export type VChildNode = VNode | string

export type VNodeList = Array<VChildNode>

export interface Component<TProps extends PropMap = PropMap> {
    (props: TProps, children: VNodeList): VNode<TProps>
}

const noType: string = ""
const noProps: PropMap = {}
const noChildren: VNodeList = []
const noNode: VNode = {
    type: noType,
    props: noProps,
    children: noChildren
}

export function h<TProps extends PropMap = PropMap>(
    type: Component<TProps> | string,
    props?: TProps,
    ...children: Array<VChildNode | number | boolean | null | undefined>
): VNode<TProps> {

    // flatten potentially nested arrays of child nodes:

    let i = 0

    while (i < children.length) {
        const child = children[i] as any

        if (Array.isArray(child)) {
            children.splice(i, 1, ...child)
        } else {
            if (typeof child === "number") {
                children[i] = "" + child
            } else if (child === true || child === false || child === "" || child === null) {
                children[i] = undefined
                continue
            }

            i += 1
        }
    }

    let key: string | undefined

    if (props) {
        // remove key from props:

        key = props.key as string

        delete props.key

        // fold the class-attribute to a string, if given as an object or array:

        if (typeof props.class === "object") {
            const classes: any = props.class

            props.class = (classes.constructor == Object
                ? Object.keys(classes).filter(name => classes[name])
                : classes as string[]
            ).join(" ")
        }
    } else {
        props = noProps as TProps
    }

    // initialize the VNode:

    return typeof type === "string"
        ? {
            type,
            props,
            key,
            children: children as VNodeList
        }
        : type(props, children as VNodeList)
}

// TODO change the exported function signature (this is compatible with Picodom for now, to satisfy the tests)
export function patch(parent: Element, newNode: VNode, oldNode?: VNode) {
    const callbacks: Function[] = []

    if (!parent.childNodes[0]) {
        parent.appendChild(createTarget(newNode, callbacks))
    }

    patchTarget(parent.childNodes[0] as TargetNode, parent, parent.childNodes[1], newNode, callbacks)

    callbacks.map(callback => callback())
}

/**
 * Creates a new Target Node
 */
function createTarget(vNode: VChildNode, callbacks: Function[]): TargetNode {
    if (typeof vNode !== "object") {
        return document.createTextNode(vNode)
    }

    const target = document.createElement(vNode.type) as any as TargetElement

    target._vNode = {
        type: vNode.type,
        props: noProps,
        key: vNode.key,
        children: noChildren
    }
    
    const create = vNode.props.oncreate

    if (create) {
        callbacks.push(() => create(target))
    }

    return target
}

/**
 * Apply properties and attributes to a given Target Element
 */
function updateTarget(target: TargetElement, newProps: PropMap) {
    const oldProps = target._vNode
        ? target._vNode.props
        : noProps

    // remove old props:

    for (const name in oldProps) {
        if (!(name in newProps)) {
            if (name in target) {
                (<any>target)[name] = null
            }

            target.removeAttribute(name)
        }
    }

    // apply new or updated props:

    for (const name in newProps) {
        if (name === "oncreate" || name === "onupdate" || name === "onremove" || name === "ondestroy") {
            continue
        }

        if (oldProps[name] !== newProps[name]) {
            const value = newProps[name]

            if (value == null || value === false) {
                target.removeAttribute(name)
            } else if (name in target) {
                (<any>target)[name] = value
            } else {
                target.setAttribute(name, value === true ? "" : value as string)
            }
        }
    }

    target._vNode.props = newProps
}

/**
 * Trigger the `ondestroy` hook for a given VNode and any applicable children
 */
function notifyDestroyed(vnode: VNode) {
    if (vnode.props.ondestroy) {
        vnode.props.ondestroy(vnode)
    }

    for (const child of vnode.children) {
        if (typeof child === "object") {
            notifyDestroyed(child)
        }
    }
}

/**
 * Destroys a given Target Element
 */
function destroyTarget(target: TargetElement) {
    notifyDestroyed(target._vNode)

    target.parentElement!.removeChild(target)

    delete target._vNode
}

/**
 * Remove and conditionally destroy a given Target Element
 */
function removeTarget(target: TargetElement, callbacks: Function[]) {
    const vNode = target._vNode
    const remove = vNode.props.onremove

    if (remove) {
        callbacks.push(() => remove(destroy))
        target._removed = true
    } else {
        destroy()
    }

    function destroy() {
        destroyTarget(target)
    }
}

function max(a: number, b: number): number {
    return a > b ? a : b
}

function isTargetElement(node: Node): node is TargetElement {
    return "_vNode" in node
}

function isTargetNode(node: Node): node is TargetNode {
    return (isTargetElement(node) && !node._removed) || node instanceof Text // TODO QA: currently, all Text Nodes are managed
}

function isCompatible(node: Node, vNode: VChildNode): node is TargetNode {
    return typeof vNode === "object"
        ? isTargetElement(node) && node._vNode.type === vNode.type
        : node instanceof Text
}

/**
 * Patches a given Target Node
 * 
 * If the target Node is undefined, the parent and nextSibling references will be used
 * to position a newly created Node, which will be returned.
 */
function patchTarget(target: TargetNode | undefined, parent: Element | undefined, nextSibling: Node | undefined, vNode: VChildNode, callbacks: Function[]): Node {

    // create or replace incompatible Target Node as needed:

    if (!target) {
        target = createTarget(vNode, callbacks)
    } else if (!isCompatible(target, vNode)) {
        removeTarget(target, callbacks)
        target = createTarget(vNode, callbacks)
    }

    // position the Target Node:
    
    if (parent && target.nextSibling !== nextSibling) {
        parent.insertBefore(target, nextSibling!)
    }

    // update the Target Node:
    
    if (typeof vNode === "string") {
        target.textContent = vNode as string

        return target
    }

    target = target as TargetElement // TODO QA: somewhat imperfect, but we known from the `typeof vNode` check above that the target is not a Text Node

    updateTarget(target, vNode.props)

    // create a list of existing child Nodes, and a map of pre-existing keyed Target Elements:

    const
        childNodes: Node[] = [],
        keyedTargetElements: { [key: string]: TargetElement } = {}

    if (parent) {
        for (let i = 0; i < parent.childNodes.length; i++) {
            const node = parent.childNodes[i]

            childNodes.push(node)

            if (isTargetElement(node)) {
                const key = node._vNode.key
                
                if (key) {
                    keyedTargetElements[key] = node
                }
            }
        }
    }

    // recursively patch children in backwards order:

    const length = max(childNodes.length, vNode.children.length)

    for (let i = length; i--; ) {
        const childNode = childNodes[i]

        if (!isTargetNode(childNode)) {
            continue // ignore non-Target Nodes (including those flagged as _removed)
        }

        const childVNode = isTargetElement(childNode) ? childNode._vNode : undefined
        const childKey = childVNode ? childVNode.key : undefined

        if (childKey && keyedTargetElements[childKey]) {
            patchTarget(childNode, target, childNodes[i+1], childVNode!, callbacks)
        } else if (childVNode) {
            patchTarget(undefined, target, childNodes[i+1], childVNode, callbacks)
        }
    }

    // remove any remaining keyed Target Elements:

    for (const key in keyedTargetElements) {
        removeTarget(keyedTargetElements[key], callbacks)
    }

    return target
}
