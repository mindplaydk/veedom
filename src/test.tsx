import { h, patch, VNode, VNodeList } from "./veedom"
import * as test from "tape"
import { JSDOM } from "jsdom"

declare global {
    namespace NodeJS {
        interface Global {
            document: Document
        }
    }
}

test("can create nodes", assert => {
    assert.deepEquals(
        <div></div>,
        {
            type: "div",
            props: {},
            key: undefined,
            children: []
        },
        "without attributes"
    )

    assert.deepEqual(
        <div a="A" b="B"></div>,
        {
            type: "div",
            props: { a: "A", b: "B" },
            key: undefined,
            children: []
        },
        "with attributes"
    )

    assert.deepEqual(
        <div>foo</div>,
        {
            type: "div",
            props: {},
            key: undefined,
            children: ["foo"]
        },
        "with text node"
    )

    assert.deepEqual(
        <div>{123}{true}{false}{null}{undefined}{""}{"foo"}</div>,
        {
            type: "div",
            props: {},
            key: undefined,
            children: ["123", undefined, undefined, undefined, undefined, undefined, "foo"]
        },
        "with text and non-text nodes"

        // NOTE: boolean nodes are translated to nulls, in order to preserve
        //       ordering of conditional child-elements - for example, this
        //       lets you use conditionals such as { value && <input value={value} /> }
        //       which evaluate to an ignored literal false value or a VNode instance
    )

    assert.deepEqual(
        <div id="A">
            <div id="A1">
                <div id="A1A"></div>
                <div id="A1B"></div>
            </div>
            <div id="A2">
                <div id="A2A"></div>
            </div>
        </div>,
        {
            type: "div",
            props: { id: "A" },
            key: undefined,
            children: [
                {
                    type: "div",
                    props: { id: "A1" },
                    key: undefined,
                    children: [
                        {
                            type: "div",
                            props: { id: "A1A" },
                            key: undefined,
                            children: []
                        },
                        {
                            type: "div",
                            props: { id: "A1B" },
                            key: undefined,
                            children: []
                        }
                    ]
                },
                {
                    type: "div",
                    props: { id: "A2" },
                    key: undefined,
                    children: [
                        {
                            type: "div",
                            props: { id: "A2A" },
                            key: undefined,
                            children: []
                        }
                    ]
                }
            ]
        },
        "with child structures"
    )

    assert.deepEqual(
        <div>{[[<div id="a" />, <div id="b" />], <div id="c" />]}<div id="d" /></div>,
        {
            type: "div",
            props: {},
            key: undefined,
            children: [
                { type: "div", props: { id: "a" }, key: undefined, children: [] },
                { type: "div", props: { id: "b" }, key: undefined, children: [] },
                { type: "div", props: { id: "c" }, key: undefined, children: [] },
                { type: "div", props: { id: "d" }, key: undefined, children: [] }
            ]
        },
        "with nested child structures"
    )

    const Hello = (props: { id: string }, children: VNodeList) => <div id={props.id}>{children}</div>

    assert.deepEqual(
        <Hello id="a">b</Hello>,
        {
            type: "div",
            props: { id: "a" },
            key: undefined,
            children: ["b"]
        },
        "with component receiving props and children"
    )

    assert.end()
})

test("can patch the DOM", function (assert) {
    const checks: Promise<void>[] = []

    function check(msg: string, steps: Array<{ node: VNode, html: string, before?: Function }>) {
        let oldNode: VNode | undefined

        const { document } = new JSDOM().window;

        global.document = document

        for (let i=0; i<steps.length; i++) {
            let { node: newNode, html, before } = steps[i]

            if (before) {
                before()
            }

            assert.comment(`${msg} (${i+1}/${steps.length})`)

            patch(document.body, newNode, oldNode)
            
            assert.equal(document.body.innerHTML, html)

            oldNode = newNode
        }
    }

    check("update/add/remove attributes", [
        {
            node: <div a="1" b="2" c="3"><div d="4" e="5" f="6"></div></div>,
            html: `<div a="1" b="2" c="3"><div d="4" e="5" f="6"></div></div>`
        },
        {
            node: <div b="2" c="7" h="9"><div e="5" f="8" i="10"></div></div>,
            html: `<div b="2" c="7" h="9"><div e="5" f="8" i="10"></div></div>`
        }
    ])

    check("render/add/remove child elements", [
        {
            node: <div id="a"></div>,
            html: `<div id="a"></div>`
        },
        {
            node: <div id="a"><div id="b1"><div id="b1a"></div><div id="b1b"></div></div><div id="b2"><div id="b2a"></div><div id="b2b"></div></div></div>,
            html: `<div id="a"><div id="b1"><div id="b1a"></div><div id="b1b"></div></div><div id="b2"><div id="b2a"></div><div id="b2b"></div></div></div>`
        },
        {
            node: <div id="a"></div>,
            html: `<div id="a"></div>`
        }
    ])

    check("render/reuse keyed child elements", [
        {
            node: <div><header></header><div key="1" id="a"></div><div key="2" id="b"></div><footer></footer></div>,
            html: `<div><header></header><div id="a"></div><div id="b"></div><footer></footer></div>`
        },
        {
            node: <div><header></header><div key="3" id="c"></div><div key="2" id="d"></div><div key="4" id="e"></div><footer></footer></div>,
            html: `<div><header></header><div id="c"></div><div id="d"></div><div id="e"></div><footer></footer></div>`
        }
    ])

    check("remove unused trailing nodes", [
        {
            node: <div><div id="a"></div><div id="b"></div></div>,
            html: `<div><div id="a"></div><div id="b"></div></div>`
        },
        {
            node: <div><div id="a"></div></div>,
            html: `<div><div id="a"></div></div>`
        }
    ])

    function test_life_cycle_events() {
        let log: string[] = []
        let updates = 0

        const view = (with_inner: boolean) =>
            <div>
                {with_inner &&
                    <div id="1" oncreate={ el => log.push(`create parent ${el.id}`) } onupdate={ el => log.push(`update parent ${el.id}`) } onremove={ remove => { log.push(`remove parent`); remove() } } ondestroy={ node => log.push(`destroy parent ${node.props.id}`) }>
                        <div id="2" oncreate={ el => log.push(`create child ${el.id}`) } onupdate={ el => log.push(`update child ${el.id}`) } onremove={ remove => { log.push(`remove child`); remove() } } ondestroy={ node => log.push(`destroy child ${node.props.id}`) }>
                        </div>
                    </div>
                }
            </div>

        const before = () => log.push(`cycle ${++updates}`)

        check("publish life-cycle events", [
            {
                node: view(true),
                html: `<div><div id="1"><div id="2"></div></div></div>`,
                before
            },
            {
                node: view(true),
                html: `<div><div id="1"><div id="2"></div></div></div>`,
                before
            },
            {
                node: view(false),
                html: `<div></div>`,
                before
            }
        ])

        assert.deepEqual(log, [
            'cycle 1', 'create parent 1', 'update parent 1', 'create child 2', 'update child 2',
            'cycle 2', 'update parent 1', 'update child 2',
            'cycle 3', 'remove parent', 'destroy parent 1', 'destroy child 2'
        ])
    }

    test_life_cycle_events()

    function test_deferred_removal() {
        let _remove: Function

        const initial =
            <div>
                <div id="a" key="a"></div>
                <div id="b" key="b" onremove={ remove => _remove = remove }></div>
                <div id="c" key="c"></div>
            </div>

        const removed =
            <div>
                <div id="a" key="a"></div>
                <div id="c" key="c"></div>
            </div>

        const initial_html = `<div><div id="a"></div><div id="b"></div><div id="c"></div></div>`

        check("maintain order during deferred removal", [
            {
                node: initial,
                html: initial_html
            },
            {
                node: removed,
                html: initial_html // removal has been deferred
            },
            {
                before: () => _remove(), // trigger deferred removal
                node: removed,
                html: `<div><div id="a"></div><div id="c"></div></div>`
            }
        ])

    }

    test_deferred_removal()
    assert.end()
})
